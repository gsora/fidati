module github.com/gsora/fidati

go 1.15

require (
	github.com/f-secure-foundry/tamago v0.0.0-20201019091057-9fc569523656
	github.com/rakyll/statik v0.1.7
	github.com/stretchr/testify v1.6.1
)
